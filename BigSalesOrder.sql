CREATE VIEW BigSalesOrder AS
	SELECT
		orderNumber, ROUND(total, 2) AS total
	FROM
		SalePerOrder
	WHERE
		total > 60000;
